export class Food {
  constructor(
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  ) {
    this.foodID = foodID;
    this.tenMon = tenMon;
    this.loai = loai;
    this.giaMon = giaMon;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhMon = hinhMon;
    this.moTa = moTa;
  }
  quyDoiTienTe(){
    return parseFloat(this.giaMon).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })
  }
  tinhGiaKM() {
    return parseFloat((this.giaMon * (100 - this.khuyenMai) / 100).toFixed(0)).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
  }
}
