export let renderFoodList = (foodArr) => {
  let contentHTML = "";
  foodArr.forEach((item) => {
    let { foodID, tenMon, loai, khuyenMai, tinhTrang } = item;
    let contentTr = `<tr>
    <td>${foodID}</td>
    <td>${tenMon}</td>
    <td>${loai == "true" ? "Mặn" : "Chay"}</td>
    <td>${item.quyDoiTienTe()}</td>
    <td>${khuyenMai} %</td>
    <td>${item.tinhGiaKM()}</td>
    <td>${tinhTrang == "true" ? "Còn" : "Hết"}</td>
    <td>
        <i onclick="suaMon(${foodID})" class="btn text-primary fa fa-edit"></i>
        <i onclick="xoaMonAn(${foodID})" class="btn text-danger fa fa-trash-alt"></i>
    </td>
    </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};

export function getInfo() {
  let foodID = document.getElementById("foodID").value*1;
  let tenMon = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let giaMon = document.getElementById("giaMon").value*1;
  let khuyenMai = document.getElementById("khuyenMai").value*1;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;
  return {
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
}
