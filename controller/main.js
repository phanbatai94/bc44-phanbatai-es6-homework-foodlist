import { renderFoodList, getInfo } from "./controller.js";
import { Food } from "../model/model.js";
//fetch data từ sv và render
const BASE_URL = "https://64561da82e41ccf169140db7.mockapi.io/Food";
let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      let foodArr = res.data.map((item) => {
        //destructuring
        let { name, type, discount, img, desc, price, status, id } = item;
        //new obj
        let food = new Food(id, name, type, price, discount, status, img, desc);
        //return
        return food;
      });
      renderFoodList(foodArr);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchFoodList();

//window có tác dụng khi trình duyệt chạy mỗi lần khai báo script type="module"
window.xoaMonAn = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};

window.themMon = () => {
  let data = getInfo();

  let newFood = {
    name: data.tenMon,
    type: data.loai,
    discount: data.khuyenMai,
    img: data.hinhMon,
    desc: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };

  axios({
    url: BASE_URL,
    method: "POST",
    data: newFood,
  })
    .then((res) => {
      fetchFoodList();
      $("#exampleModal").modal("hide");
      resetFoodForm();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.suaMon = (id) => {
  //lấy data từ api dựa vào id
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");
      let { id, name, type, price, img, discount, desc, status } = res.data;
      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value = type;
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount;
      document.getElementById("tinhTrang").value = status;
      document.getElementById("hinhMon").value = img;
      document.getElementById("moTa").value = desc;
    })
    .catch((err) => {
      console.log(err);
    });
};

window.capNhatMon = (id) => {
  let data = getInfo();

  let newFoodInfo = {
    name: data.tenMon,
    type: data.loai,
    discount: data.khuyenMai,
    img: data.hinhMon,
    desc: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };

  axios({
    url: `${BASE_URL}/${id}`,
    method: "PUT",
    data: newFoodInfo,
  })
    .then((res) => {
      fetchFoodList();
      $("#exampleModal").modal("hide");
      resetFoodForm();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.locMon = async (loaiMon) => {
  let res = await axios.get(BASE_URL);
  let dsMon = res.data.map((item) => {
    let { name, type, discount, img, desc, price, status, id } = item;
    let monAn = new Food(id, name, type, price, discount, status, img, desc);
    return monAn;
  });
  let dsMonTheoLoai = dsMon.filter((item) => item.loai == loaiMon);
  renderFoodList(dsMonTheoLoai.length ? dsMonTheoLoai : dsMon);
};

document.getElementById("selLoai").addEventListener("change", function () {
  locMon(this.value);
});

window.resetFoodForm = () => {
  document.getElementById("foodForm").reset();
};

document.getElementById("btnThem").addEventListener("click", resetFoodForm);
